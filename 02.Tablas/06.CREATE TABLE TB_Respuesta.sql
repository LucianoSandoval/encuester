USE Encuester
GO

CREATE TABLE TB_Respuesta
(
	IDRespuesta INT IDENTITY(1,1) PRIMARY KEY,
	Respuesta VARCHAR(500),
	OpcionId INT,
	PreguntaId INT,
	CONSTRAINT FK_OpcResp FOREIGN KEY(OpcionId) REFERENCES TB_Opcion(IdOpcion),
	CONSTRAINT FK_PregResp FOREIGN KEY(PreguntaId) REFERENCES TB_Pregunta(IdPregunta)
)