USE Encuester
GO

CREATE TABLE TB_Pregunta
(
	IdPregunta INT IDENTITY(1,1) PRIMARY KEY,
	Pregunta VARCHAR(300) NOT NULL,
	TipoId INT,
	EncuestaId INT,
	CONSTRAINT FK_TipoPreg FOREIGN KEY (TipoId)REFERENCES TB_TipoPregunta(IdTipo),
	CONSTRAINT FK_EncPreg FOREIGN KEY (EncuestaId)REFERENCES TB_Encuesta(IdEncuesta)
)