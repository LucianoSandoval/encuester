USE Encuester
GO

CREATE TABLE TB_Encuesta
(
	IdEncuesta INT IDENTITY(1,1) PRIMARY KEY,
	Encuesta VARCHAR(100) NOT NULL,
	Descripcion VARCHAR(500) NOT NULL,
	Link VARCHAR(500) NOT NULL,
	Estatus BIT DEFAULT 1,
	UsuarioId INT,
	CONSTRAINT FK_UsuarioEncuesta FOREIGN KEY (UsuarioId) REFERENCES TB_Usuario(IdUsuario)
)